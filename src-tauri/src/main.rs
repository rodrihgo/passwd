// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
//#[tauri::command]
//fn greet(name: &str) -> String {
//    format!("Hello, {}! You've been greeted from Rust!", name)
//}

// fn main() {
    // tauri::Builder::default()
    //     .invoke_handler(tauri::generate_handler![greet])
    //     .run(tauri::generate_context!())
    //     .expect("error while running tauri application");
// }

use tokio::net::TcpListener;
use tokio_tungstenite::accept_async;
use futures_util::{stream::StreamExt, SinkExt};
use tokio_tungstenite::tungstenite::protocol::Message;
use tokio::sync::broadcast;
use std::thread;
use std::time::Duration;
// use tauri::CursorIcon::Text;

#[tokio::main]
async fn main() {
    let try_socket = TcpListener::bind("127.0.0.1:8080").await.expect("Failed to bind");
    println!("WebSocket server listening on ws://127.0.0.1:8080");
    let (tx, _rx) = broadcast::channel(10); // 10 es el tamaño del buffer del canal
    // while let Ok((stream, _)) = listener.accept().await {
    //     tokio::spawn(handle_connection(stream));
    // }

    while let Ok((stream, _)) = try_socket.accept().await {
        let ws_stream = accept_async(stream).await.expect("Error during the websocket handshake");
        println!("New WebSocket connection");

        let (mut write, mut read) = ws_stream.split();
        let tx_clone = tx.clone(); // Clonamos el transmisor para este cliente
        let mut rx = tx.subscribe(); // Cada cliente tiene su propio receptor

        // Procesar mensajes entrantes y retransmitirlos
        tokio::spawn(async move {
            while let Some(Ok(message)) = read.next().await {
                // Retransmitir el mensaje a todos los clientes

                let _ = tx_clone.send(message.clone());

                // También puedes manejar los mensajes aquí si es necesario
            }
        });

        // Escuchar mensajes del canal y enviarlos a este cliente
        tokio::spawn(async move {
            while let Ok(message) = rx.recv().await {
                let msg: &str = message.to_text().unwrap();
                println!("Received a message: {}", msg);
                // Message::Text(pong.to_string())).await.unwrap();
                let _arr  = msg.split("");
                for i in _arr{
                    let result = write.send(Message::Text(i.to_string())).await;
                    if result.is_err() {
                        println!("Error cliente desconectado?");
                        break; // Si hay un error al enviar, probablemente el cliente se desconectó
                    }
                    thread::sleep(Duration::from_millis(500));
                }
                if write.send(Message::Text("DONE".to_string())).await.is_err() {
                    println!("Error cliente desconectado?");
                    break; // Si hay un error al enviar, probablemente el cliente se desconectó
                }
            }
        });
    }
}

async fn handle_connection(stream: tokio::net::TcpStream) {
    let ws_stream = accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");
    println!("New WebSocket connection");

    let (mut write, mut read) = ws_stream.split();

    while let Some(message) = read.next().await {
        let message: Message = message.unwrap();
        let msg: &str = message.to_text().unwrap();
        println!("Received a message: {}", msg);
        // Aquí puedes manejar mensajes entrantes y enviar respuestas
        //Enviar devuelta otro mensaje:
        let pong: &str = "Hola desde Rust!";
        // let arr  = pong.split("");
        write.send(Message::Text(pong.to_string())).await.unwrap();
        // for i in arr{
        //     write.send(Message::Text(i.to_string())).await.unwrap();

        // }
    }
}



// Suponiendo que `write` es tu canal de escritura obtenido del `split`.
// Para enviar un mensaje, necesitarás usar el método `send` en el objeto adecuado.
// Aquí está el ejemplo corregido:
// async fn send_message(write: &mut SplitSink<WebSocketStream<TcpStream>, Message>) {
//     write.send(Message::Text("Hola desde Rust!".to_string())).await.unwrap();
// }


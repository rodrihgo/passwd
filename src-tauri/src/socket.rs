use tokio::net::TcpListener;
use tokio_tungstenite::accept_async;
use futures_util::stream::StreamExt;

#[tokio::main]
async fn main() {
    let try_socket = TcpListener::bind("127.0.0.1:8080").await;
    let listener = try_socket.expect("Failed to bind");
    println!("WebSocket server listening on ws://127.0.0.1:8080");

    while let Ok((stream, _)) = listener.accept().await {
        tokio::spawn(handle_connection(stream));
    }
}

async fn handle_connection(stream: tokio::net::TcpStream) {
    let ws_stream = accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");
    println!("New WebSocket connection");

    let (write, mut read) = ws_stream.split();

    while let Some(message) = read.next().await {
        let message = message.unwrap();
        println!("Received a message: {}", message.to_text().unwrap());
        // Aquí puedes manejar mensajes entrantes y enviar respuestas
    }
}
